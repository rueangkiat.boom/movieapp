import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import Movie from "./component/Movie";
import Basket from "./component/ฺBasket";
import { useState } from "react";

function App() {
  const [buyList, setBuyList] = useState({});
  const pull_data = (data) => {
    setBuyList(data);
  };

  return (
    <div className="App">
      <Routes>
        <Route
          path="/"
          element={<Movie func={pull_data} buyMovies={buyList} />}
        ></Route>
        <Route
          path="/buy"
          element={<Basket func={pull_data} buyMovies={buyList} mp />}
        ></Route>
      </Routes>
    </div>
  );
}

export default App;
