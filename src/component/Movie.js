import { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { BrowserRouter as Router, useNavigate } from "react-router-dom";
import "./Movie.css";


function Movie(props) {
  let navigate = useNavigate();
  const [buyMovies, setBuyMovies] = useState(props.buyMovies);
  const [movies, setMovies] = useState([]);
  const [activeMovies, setActiveMovies] = useState([]);
  const key = "8978b5f356d4f44066bc5e3a516e8333";
  const baseImageUrl = "https://image.tmdb.org/t/p/w500";
  const [inputsearch, setInputSearch] = useState("");
  const sum = Object.keys(buyMovies).reduce((acc, movieId) => {
    return acc + buyMovies[movieId].qty;
  }, 0);
  const update = useCallback(
    (movieId, qty) => {
      console.log("update", movieId, qty);
      console.log(buyMovies);
      if (qty <= 0) {
        delete buyMovies[movieId];
        setBuyMovies({
          ...buyMovies,
        });
      } else {
        // get movie
        const movie = activeMovies.find((movie) => movie.id === movieId);
        setBuyMovies({
          ...buyMovies,
          [movieId]: {
            ...movie,
            qty: qty,
          },
        });
      }
    },
    [activeMovies, buyMovies]
  );

  const check = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${key}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate`
      )
      .then((res) => {
        setMovies(res.data.results);
        setActiveMovies(res.data.results);
      })
      .catch((err) => console.log(err));
  };
  useEffect(() => {
    check();
  }, []);

  useEffect(() => {
    if (inputsearch.length > 0) {
      const searchMovies = movies.filter((movie) => {
        return movie.title.toLowerCase().search(inputsearch) > -1;
      });
      console.log({ todo: inputsearch, searchMovies });
      setActiveMovies(searchMovies);
    } else {
      setActiveMovies(movies);
    }
  }, [inputsearch, movies]);
  function handleInputChange(e) {
    setInputSearch(e.target.value);
  }
  const handleSubmit = () => {
    props.func(buyMovies);
    navigate("/buy");
  };

  return (
    <div className="body">
      <div className="head">
        <div className="logo">
          <img src={require("./logo.png")} onClick={handleSubmit}></img>
          <h1>MovieApp</h1>
          <input
            className="input"
            type="text"
            placeholder="ค้นหาหนังที่ต้องการ"
            value={inputsearch}
            onChange={handleInputChange}
          ></input>
        </div>

        <div className="boxbasket">
          <img
            src={`http://simpleicon.com/wp-content/uploads/basket.png`}
            onClick={handleSubmit}
          ></img>
          <h2>{sum}</h2>
        </div>
      </div>
      <div className="boxcontent">
        <div className="content">
          {activeMovies.map((movie) => (
            <div className="boxmovie">
              <img
                src={`${baseImageUrl}${movie.poster_path}`}
                alt="Frist"
                value={movie.id}
              ></img>
              <h1>{movie.title}</h1>
              <h1
                className="cart"
                onClick={() => {
                  update(
                    movie.id,
                    buyMovies[movie.id] ? buyMovies[movie.id].qty + 1 : 1
                  );
                }}
              >
                ADD TO CART
              </h1>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
export default Movie;
