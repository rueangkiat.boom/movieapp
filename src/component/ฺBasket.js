import {  useCallback, useState } from "react";

import { BrowserRouter as Router, useNavigate } from "react-router-dom";

function Basket(props) {
  let navigate = useNavigate();

  const baseImageUrl = "https://image.tmdb.org/t/p/w500";
  const [buylist, setBuyList] = useState(props.buyMovies);

  const handleSubmit = () => {
    props.func(buylist);
    navigate("/");
  };
  const update = useCallback(
    (movieId, qty) => {
      console.log("update", movieId, qty);
      if (qty <= 0) {
        delete buylist[movieId];
        setBuyList({
          ...buylist,
        });
      } else {
        // get movie
        const movie = Object.values(buylist).find(
          (movie) => movie.id === movieId
        );
        setBuyList({
          ...buylist,
          [movieId]: {
            ...movie,
            qty,
          },
        });
      }
    },
    [buylist]
  );

  return (
    <div>
      <div className="head">
        <div className="logo">
          <img src={require("./logo.png")}></img>
          <h1>MovieApp</h1>
        </div>
        <h1 className="listbuy">รายการสินค้าที่คุณเลือก</h1>
        <h1 className="home" onClick={handleSubmit}>
          กลับสู่หน้าหลัก
        </h1>
      </div>
      <div className="boxcontentbasket">
        <div className="contentbasket">
          {Object.values(buylist).map((movie) => (
            <div className="boxmoviebasket">
              <img
                src={`${baseImageUrl}${movie.poster_path}`}
                alt="Frist"
              ></img>

              <h1>{movie.title}</h1>
              <div className="inforbuy">
                <button
                  className="update"
                  onClick={() =>
                    update(
                      movie.id,
                      buylist[movie.id] ? buylist[movie.id].qty - 1 : 0
                    )
                  }
                >
                  -
                </button>
                <h1>{movie.qty}</h1>
                <button
                  className="update"
                  onClick={() =>
                    update(
                      movie.id,
                      buylist[movie.id] ? buylist[movie.id].qty + 1 : 1
                    )
                  }
                >
                  +
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
export default Basket;
